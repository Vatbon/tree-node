import treenode.treenode.TreeNode;

public class Main {
    public static void main(String[] args) {
        if (args.length < 3) {
            return;
        }
        String name = args[0];
        int port = Integer.parseInt(args[1]);
        int lossPercent = Integer.parseInt(args[2]);
        String ipNeighbour = null;
        int portNeigh = -1;
        if (args.length == 5) {
            ipNeighbour = String.copyValueOf(args[3].toCharArray());
            portNeigh = Integer.parseInt(args[4]);
        }
        TreeNode treeNode = new TreeNode(name, lossPercent, port);
        if (ipNeighbour != null && portNeigh != -1) {
            treeNode.connect(ipNeighbour, portNeigh);
        }
        treeNode.start();
    }
}
