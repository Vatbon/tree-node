package treenode.protocol;

import java.nio.ByteBuffer;

public class NodeProtocol {
    public enum PackageType {
        CONNECTION, CONNECTION_ACK, MESSAGE, MESSAGE_ACK, PING, PARENT, PARENT_ACK
    }

    private static final int CONNECTION = 1;
    private static final int CONNECTION_ACK = 2;
    private static final int MESSAGE = 3;
    private static final int MESSAGE_ACK = 4;
    private static final int PARENT = 5;
    private static final int PARENT_ACK = 6;
    private static final int PING = 127;

    static byte getByteFromPackageType(PackageType type) {
        switch (type) {
            case CONNECTION:
                return CONNECTION;
            case CONNECTION_ACK:
                return CONNECTION_ACK;
            case MESSAGE:
                return MESSAGE;
            case MESSAGE_ACK:
                return MESSAGE_ACK;
            case PING:
                return PING;
            case PARENT:
                return PARENT;
            case PARENT_ACK:
                return PARENT_ACK;
            default:
                return -1;
        }
    }

    static PackageType getPackageTypeFromByte(byte code) throws PackageTypeNotFoundException {
        switch (code) {
            case CONNECTION:
                return PackageType.CONNECTION;
            case CONNECTION_ACK:
                return PackageType.CONNECTION_ACK;
            case MESSAGE:
                return PackageType.MESSAGE;
            case MESSAGE_ACK:
                return PackageType.MESSAGE_ACK;
            case PING:
                return PackageType.PING;
            case PARENT:
                return PackageType.PARENT;
            case PARENT_ACK:
                return PackageType.PARENT_ACK;
            default:
                throw new PackageTypeNotFoundException();
        }
    }

    public static PackageType getPackageTypeFromHead(byte[] bytes) throws EmptyByteArrayException, PackageTypeNotFoundException {
        if (bytes.length > 0)
            return getPackageTypeFromByte(bytes[0]);
        else
            throw new EmptyByteArrayException();
    }

    public static int getPacketLengthFromHead(byte[] head) {
        if (head.length >= 5)
            return ByteBuffer.wrap(head, 1, 4).getInt();
        else
            return -1;
    }
}

