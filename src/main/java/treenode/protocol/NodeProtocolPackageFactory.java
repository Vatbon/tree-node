package treenode.protocol;

import treenode.util.HashFactory;

import java.nio.ByteBuffer;

import static treenode.protocol.NodeProtocol.PackageType;

public class NodeProtocolPackageFactory {

    private NodeProtocolPackageFactory() {
    }

    /**
     * @param type      Type of a packet
     * @param bytes     body for a packet.
     *                  Words in MESSAGE and MESSAGE_ACK.
     *                  Name of a node in CONNECTION.
     *                  IpAddress of a node in PARENT.
     * @param nameBytes name of a node. Used in making MESSAGE packets.
     *                  port of a node. Used in making PARENT packets.
     * @return Packet
     */
    public static Packet makePackage(PackageType type, byte[] bytes, byte[] nameBytes) throws EmptyByteArrayException {
        byte typeByte = NodeProtocol.getByteFromPackageType(type);
        byte[] guid;
        switch (type) {
            case MESSAGE:
                if (bytes.length == 0)
                    throw new EmptyByteArrayException();
                guid = HashFactory.createChecksum();
                return new Packet(ByteBuffer.allocate(9 + nameBytes.length + HashFactory.HASHSIZE + bytes.length)
                        .put(typeByte)
                        .putInt(4 + HashFactory.HASHSIZE + nameBytes.length + bytes.length)
                        .putInt(nameBytes.length)
                        .put(nameBytes)
                        .put(guid)
                        .put(bytes).array());

            case MESSAGE_ACK:
                if (bytes.length == 0)
                    throw new EmptyByteArrayException();
                if (bytes.length != HashFactory.HASHSIZE)
                    return null;
                return new Packet(ByteBuffer.allocate(5 + HashFactory.HASHSIZE)
                        .put(typeByte)
                        .putInt(HashFactory.HASHSIZE)
                        .put(bytes).array());

            case CONNECTION:
                if (bytes.length == 0)
                    throw new EmptyByteArrayException();
                return new Packet(ByteBuffer.allocate(5 + bytes.length).put(typeByte).putInt(bytes.length).put(bytes).array());

            case PARENT:
                if (bytes.length == 0)
                    throw new EmptyByteArrayException();
                return new Packet(ByteBuffer.allocate(9 + bytes.length)
                        .put(typeByte)
                        .putInt(4 + bytes.length)
                        .put(nameBytes)
                        .put(bytes).array());
            case CONNECTION_ACK:
            case PARENT_ACK:
            case PING:
                return new Packet(ByteBuffer.allocate(5).put(typeByte).putInt(0).array());
            default:
                return null;
        }
    }
}
