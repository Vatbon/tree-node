package treenode.protocol;

import treenode.util.HashFactory;

import java.nio.ByteBuffer;

public class Packet {
    private byte[] bytes;

    public byte[] getBytes() {
        return bytes;
    }

    public Packet(byte[] bytes) throws EmptyByteArrayException {
        if (bytes.length == 0)
            throw new EmptyByteArrayException();
        this.bytes = bytes.clone();
    }

    public NodeProtocol.PackageType getPackageType() throws PackageTypeNotFoundException {
        return NodeProtocol.getPackageTypeFromByte(bytes[0]);
    }

    /**
     * @return InetAddress of a node
     * @throws PackageTypeNotFoundException
     */
    public byte[] getInetAddress() throws PackageTypeNotFoundException {
        if (bytes.length > 9 && NodeProtocol.getPackageTypeFromByte(bytes[0]) == NodeProtocol.PackageType.PARENT) {
            int ipLen = ByteBuffer.wrap(bytes, 1, 4).getInt() - 4;
            byte[] res = new byte[ipLen];
            ByteBuffer.wrap(bytes, 9, ipLen).get(res, 0, ipLen);
            return res;
        } else return new byte[0];
    }

    /**
     * @return port of a node
     * @throws PackageTypeNotFoundException
     */
    public int getPort() throws PackageTypeNotFoundException {
        if (bytes.length > 9 && NodeProtocol.getPackageTypeFromByte(bytes[0]) == NodeProtocol.PackageType.PARENT) {
            return ByteBuffer.wrap(bytes, 5, 4).getInt();
        } else
            return -1;
    }

    /**
     * @return Returns body(message) of the packet. Supports only MESSAGE
     */
    public byte[] getBody() throws PackageTypeNotFoundException {
        if ((bytes.length > 5) && (NodeProtocol.getPackageTypeFromByte(bytes[0]) == NodeProtocol.PackageType.MESSAGE)) {
            int len = ByteBuffer.wrap(bytes, 1, 4).getInt();
            int nameLen = ByteBuffer.wrap(bytes, 5, 4).getInt();
            int bodyLen = len - nameLen - HashFactory.HASHSIZE - 4;
            byte[] res = new byte[bodyLen];
            ByteBuffer.wrap(bytes, 9 + nameLen + HashFactory.HASHSIZE, bodyLen)
                    .get(res, 0, bodyLen);
            return res;
        } else
            return new byte[0];
    }

    /**
     * @return Name of the node. Supports only MESSAGE
     * @throws PackageTypeNotFoundException
     */
    public byte[] getName() throws PackageTypeNotFoundException {
        if (NodeProtocol.getPackageTypeFromByte(bytes[0]) == NodeProtocol.PackageType.MESSAGE) {
            int nameLen = ByteBuffer.wrap(bytes, 5, 4).getInt();
            byte[] res = new byte[nameLen];
            if (bytes.length >= nameLen + 9) {
                ByteBuffer.wrap(bytes, 9, nameLen).get(res, 0, nameLen);
                return res;
            }
        }
        if (NodeProtocol.getPackageTypeFromByte(bytes[0]) == NodeProtocol.PackageType.CONNECTION) {
            int nameLen = ByteBuffer.wrap(bytes, 1, 4).getInt();
            byte[] res = new byte[nameLen];
            if (bytes.length >= nameLen + 5) {
                ByteBuffer.wrap(bytes, 5, nameLen).get(res, 0, nameLen);
                return res;
            }
        }
        return new byte[0];
    }

    /**
     * @return guid of the packet. Supports only MESSAGE and MESSAGE_ACK
     * @throws PackageTypeNotFoundException
     */
    public byte[] getGuid() throws PackageTypeNotFoundException {
        byte[] res;
        switch (NodeProtocol.getPackageTypeFromByte(bytes[0])) {
            case MESSAGE:
                int nameLen = ByteBuffer.wrap(bytes, 5, 4).getInt();
                if (bytes.length >= nameLen + 9 + HashFactory.HASHSIZE) {
                    res = new byte[HashFactory.HASHSIZE];
                    ByteBuffer.wrap(bytes, 9 + nameLen, HashFactory.HASHSIZE).get(res, 0, HashFactory.HASHSIZE);
                    return res;
                } else
                    return new byte[0];

            case MESSAGE_ACK:
                if (bytes.length >= HashFactory.HASHSIZE + 1) {
                    res = new byte[HashFactory.HASHSIZE];
                    ByteBuffer.wrap(bytes, 5, HashFactory.HASHSIZE).get(res, 0, HashFactory.HASHSIZE);
                    return res;
                } else
                    return new byte[0];
            default:
                return new byte[0];
        }
    }
}
