package treenode.treenode;

import treenode.protocol.*;
import treenode.treenode.entities.Connection;
import treenode.treenode.entities.NodePacket;
import treenode.util.NodeLogger;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class TreeNode extends Thread {
    /*
     * check new message -> resend messages -> check stdin -> send stdin
     */
    private static final int SOTIMEOUT = 300; //in milliseconds
    private static final int LISTENDURATION = 300; //in milliseconds
    public static final long NODEOLDMILLIS = 20000; //in milliseconds
    public static long ACKOLDMILLIS = 10000; //in milliseconds
    public static long ALLOLDMILLIS = 60000;

    private List<Connection> connectionList = new ArrayList<>(); // neighbours
    private DatagramSocket socket;
    private List<NodePacket> packets = new ArrayList<>();
    private final String name;
    private final int lossPercent;
    private final Random random = new Random();
    private InetAddress needConnectToInetAddress = null;
    private int needConnectToPort = -1;
    private NodeLogger logger;
    private Connection parent = null;
    private Connection backupParent = null;

    public TreeNode(String name, int lossPercent, int port) {
        this.name = name;
        this.lossPercent = lossPercent;
        try {
            this.socket = new DatagramSocket(port);
            socket.setSoTimeout(SOTIMEOUT);
        } catch (SocketException e) {
            System.out.println("Unable to open socket on `" + port + "` port.");
        }
        this.logger = new NodeLogger();
    }

    public void connect(String ip, int port) {
        try {
            needConnectToInetAddress = InetAddress.getByName(ip);
            needConnectToPort = port;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("Local address: " + socket.getLocalAddress());
        System.out.println("Local port: " + socket.getLocalPort());
        if (needConnectToInetAddress != null && needConnectToPort != -1) {
            Connection conn = new Connection(needConnectToInetAddress, needConnectToPort);
            connectionList.add(conn);
            makeConnection(conn, false);
            System.out.println("Connected.");
        }
        /*Main loop*/
        while (true) {
            listenSocket();
            /*Check input on any text or command*/
            try {
                if (System.in.available() > 0) {
                    String input = System.console().readLine();
                    if ("quit".equals(input)) {
                        System.out.println("Exit program.");
                        break;
                    }
                    /*make message and add it into collection*/
                    Packet packet = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.MESSAGE, input.getBytes(), name.getBytes());
                    for (Connection connection : connectionList) {
                        packets.add(new NodePacket(connection, packet));
                    }
                }
            } catch (IOException | EmptyByteArrayException e) {
                e.printStackTrace();
            }
            removeOldPackets();
            removeDeadNodes();

            /*make new parent, if non presented*/
            if (parent == null && connectionList.size() > 1) {
                Connection connection = connectionList.get(0);
                if (!connection.isDead() && connection.isConnected()) {
                    parent = connection.clone();
                    /*send to parent any other node, other than itself, to later rebuild*/
                    Connection connectionForParent = connectionList.get(1);
                    try {
                        packets.add(new NodePacket(parent, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT, connectionForParent.getInetAddress().getAddress(), ByteBuffer.allocate(4).putInt(connectionForParent.getPort()).array())));
                    } catch (EmptyByteArrayException e) {
                        e.printStackTrace();
                    }

                    /*send new parent for later rebuild to everyone*/
                    for (Connection connection1 : connectionList) {
                        try {
                            if (!connection1.equals(parent))
                                packets.add(new NodePacket(connection1, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT, parent.getInetAddress().getAddress(), ByteBuffer.allocate(4).putInt(parent.getPort()).array())));
                        } catch (EmptyByteArrayException ignored) {
                        }
                    }
                }
            }
            sendPackets();
            sendPing();
        }
        connectionList.clear();
        packets.clear();
        socket.close();
    }
    
    private void sendPing() {
        for (Connection connection : connectionList) {
            try {
                sendMessage(new NodePacket(connection, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PING, null, null)));
            } catch (EmptyByteArrayException ignored) {
            }
        }
    }

    private void removeDeadNodes() {
        Iterator<Connection> iter = connectionList.iterator();
        while (iter.hasNext()) {
            Connection connection = iter.next();
            if (connection.isDead()) {
                packets.removeIf(nodePacket -> Arrays.equals(nodePacket.getConnection().getInetAddress().getAddress(), connection.getInetAddress().getAddress()) &&
                        connection.getPort() == nodePacket.getConnection().getPort());
                if (connection.equals(parent)) {
                    parent = null;
                    makeConnection(backupParent, true);
                }
                iter.remove();
            }
        }
    }

    private void sendPackets() {
        for (NodePacket packet : packets) {
            sendMessage(packet);
        }
    }

    private void removeOldPackets() {
        packets.removeIf(packet -> {
            try {
                NodeProtocol.PackageType type = packet.getPacket().getPackageType();
                boolean isAck = type.equals(NodeProtocol.PackageType.MESSAGE_ACK) ||
                        type.equals(NodeProtocol.PackageType.CONNECTION_ACK) ||
                        type.equals(NodeProtocol.PackageType.PARENT_ACK);
                return packet.isOld() && isAck;
            } catch (PackageTypeNotFoundException ignored) {
            }
            return true;
        });
    }

    private void makeConnection(Connection connection, boolean isRebuild) {
        if (connection == null)
            return;
        connection.setConnected(false);
        try {
            NodePacket packet = new NodePacket(connection, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.CONNECTION, name.getBytes(), null));
            DatagramPacket response = new DatagramPacket(new byte[5], 5, connection.getInetAddress(), connection.getPort());
            socket.setSoTimeout(SOTIMEOUT / 5);
            while (!connection.isConnected()) {
                sendMessage(packet);
                try {
                    socket.receive(response);
                    if (NodeProtocol.getPackageTypeFromHead(response.getData()).equals(NodeProtocol.PackageType.CONNECTION_ACK) ||
                            (isRebuild && NodeProtocol.getPackageTypeFromHead(response.getData()).equals(NodeProtocol.PackageType.CONNECTION))) {
                        connection.setConnected(true);
                        parent = connection;
                    }
                } catch (SocketTimeoutException ignored) {
                } catch (IOException | PackageTypeNotFoundException e) {
                    e.printStackTrace();
                }
            }
            socket.setSoTimeout(SOTIMEOUT);
        } catch (EmptyByteArrayException | SocketException e) {
            e.printStackTrace();
        }
    }

    private void listenSocket() {
        long stamp = System.currentTimeMillis();
        while (System.currentTimeMillis() - stamp < LISTENDURATION) {
            try {
                /*Read head*/
                DatagramPacket datagramPacket = new DatagramPacket(new byte[8096], 8096);
                socket.receive(datagramPacket);
                Connection connection = findConnectionOrMakeNew(datagramPacket.getAddress(), datagramPacket.getPort());
                connection.updateTime();
                Packet packet = new Packet(ByteBuffer.allocate(datagramPacket.getLength()).put(datagramPacket.getData(), 0, datagramPacket.getLength()).array());
                logger.log(new NodePacket(connection, packet), 1);
                switch (NodeProtocol.getPackageTypeFromHead(datagramPacket.getData())) {
                    case MESSAGE:
                        /*update message_ack if present*/
                        boolean isMessageAckPresented = false;
                        for (NodePacket packet1 : packets) {
                            if (packet1.getPacket().getPackageType().equals(NodeProtocol.PackageType.MESSAGE_ACK) &&
                                    Arrays.equals(datagramPacket.getAddress().getAddress(), packet1.getConnection().getInetAddress().getAddress()) &&
                                    datagramPacket.getPort() == packet1.getConnection().getPort() &&
                                    Arrays.equals(packet1.getPacket().getGuid(), packet.getGuid())) {
                                packet1.updatetime();
                                isMessageAckPresented = true;
                                break;
                            }
                        }
                        /*make reply packet*/
                        if (!isMessageAckPresented) {
                            /*add packet to be sent for every connection*/
                            for (Connection connection1 : connectionList) {
                                if (!(connection1.getInetAddress().equals(datagramPacket.getAddress()) && connection1.getPort() == datagramPacket.getPort()))
                                    packets.add(new NodePacket(connection1, packet));
                            }
                            /*show message*/
                            showMessage(packet);
                            /**/
                            packet = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.MESSAGE_ACK, packet.getGuid(), null);
                            packets.add(new NodePacket(connection, packet));
                        }
                        break;
                    case MESSAGE_ACK:
                        /*remove packet, which was received by another node*/
                        packets.removeIf(p -> {
                            try {
                                return p.getPacket().getPackageType().equals(NodeProtocol.PackageType.MESSAGE) &&
                                        Arrays.equals(p.getConnection().getInetAddress().getAddress(), datagramPacket.getAddress().getAddress()) &&
                                        p.getConnection().getPort() == datagramPacket.getPort() &&
                                        Arrays.equals(p.getPacket().getGuid(), new Packet(ByteBuffer.allocate(datagramPacket.getLength()).put(datagramPacket.getData(), 0, datagramPacket.getLength()).array()).getGuid());
                            } catch (PackageTypeNotFoundException | EmptyByteArrayException ignored) {
                            }
                            return false;
                        });
                        break;
                    case CONNECTION:
                        /*update connection_ack if presented*/
                        boolean isConnectionAckPresented = false;
                        for (NodePacket packet1 : packets) {
                            if (packet1.getPacket().getPackageType().equals(NodeProtocol.PackageType.CONNECTION_ACK) &&
                                    Arrays.equals(datagramPacket.getAddress().getAddress(), packet1.getConnection().getInetAddress().getAddress()) &&
                                    datagramPacket.getPort() == packet1.getConnection().getPort()) {
                                packet1.updatetime();
                                isConnectionAckPresented = true;
                                break;
                            }
                        }
                        /*make response*/
                        if (!isConnectionAckPresented && !connection.isConnected()) {
                            /*add connection to the list*/
                            packets.add(new NodePacket(connection, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.CONNECTION_ACK, null, null)));
                            connection.setConnected(true);
                            /*send them parent to later rebuild*/
                            if (parent != null)
                                packets.add(new NodePacket(connection, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT, parent.getInetAddress().getAddress(), ByteBuffer.allocate(4).putInt(parent.getPort()).array())));
                        }
                        break;
                    case PARENT:
                        backupParent = new Connection(InetAddress.getByAddress(packet.getInetAddress()), packet.getPort());
                        boolean isParentAckPresented = false;
                        for (NodePacket packet1 : packets) {
                            if (packet1.getPacket().getPackageType().equals(NodeProtocol.PackageType.PARENT_ACK) &&
                                    Arrays.equals(datagramPacket.getAddress().getAddress(), packet1.getConnection().getInetAddress().getAddress()) &&
                                    datagramPacket.getPort() == packet1.getConnection().getPort()) {
                                packet1.updatetime();
                                isParentAckPresented = true;
                                break;
                            }
                        }
                        /*make response*/
                        if (!isParentAckPresented) {
                            packets.add(new NodePacket(connection, NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT_ACK, null, null)));
                        }
                        break;
                    case PARENT_ACK:
                        /*remove packet, which was received by another node*/
                        packets.removeIf(p -> {
                            try {
                                return p.getPacket().getPackageType().equals(NodeProtocol.PackageType.PARENT) &&
                                        Arrays.equals(p.getConnection().getInetAddress().getAddress(), datagramPacket.getAddress().getAddress()) &&
                                        p.getConnection().getPort() == datagramPacket.getPort();
                            } catch (PackageTypeNotFoundException ignored) {
                            }
                            return false;
                        });
                        break;
                    case CONNECTION_ACK:
                    case PING:
                    default:
                        break;
                }
            } catch (PackageTypeNotFoundException | EmptyByteArrayException ignored) {
            } catch (SocketTimeoutException e) {
                return;
            } catch (IOException e) {
                System.out.println("An error occurred during listening socket");
            }
        }
    }

    private void showMessage(Packet packet) {
        try {
            System.out.println("\033[0;32m<" + String.valueOf(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(packet.getName()))) + ">:" + String.valueOf(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(packet.getBody()))) + "\033[0m");
        } catch (PackageTypeNotFoundException ignored) {
        }
    }

    private Connection findConnectionOrMakeNew(InetAddress address, int port) {
        for (Connection connection : connectionList) {
            if (Arrays.equals(connection.getInetAddress().getAddress(), address.getAddress()) && connection.getPort() == port)
                return connection;
        }
        Connection connection = new Connection(address, port);
        connectionList.add(connection);
        return connection;
    }

    private void sendMessage(NodePacket packet) {
        if (random.nextInt(100) >= lossPercent) {
            try {
                logger.log(packet, 0);
                byte[] data = packet.getPacket().getBytes();
                Connection connection = packet.getConnection();
                DatagramPacket datagramPacket = new DatagramPacket(data, data.length, connection.getInetAddress(), connection.getPort());
                socket.send(datagramPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
