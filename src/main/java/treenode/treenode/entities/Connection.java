package treenode.treenode.entities;

import treenode.treenode.TreeNode;

import java.net.InetAddress;
import java.util.Arrays;

public class Connection {

    private boolean connected = false;
    private final InetAddress inetAddress;
    private final int port;
    private long pingTime;


    public Connection(InetAddress inetAddress, int port) {
        this.inetAddress = inetAddress;
        this.port = port;
        pingTime = System.currentTimeMillis();
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public int getPort() {
        return port;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean set) {
        connected = set;
    }

    private void setPingTime(long pingTime){
        this.pingTime = pingTime;
    }

    public void updateTime() {
        pingTime = System.currentTimeMillis();
    }

    public boolean isDead() {
        return System.currentTimeMillis() - pingTime > TreeNode.NODEOLDMILLIS;
    }

    public Connection clone() {
        Connection connection = new Connection(inetAddress, port);
        connection.setPingTime(this.pingTime);
        connection.setConnected(isConnected());
        return connection;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Connection connection = (Connection) o;
        return Arrays.equals(this.getInetAddress().getAddress(), connection.getInetAddress().getAddress()) && this.getPort() == connection.getPort();
    }
}
