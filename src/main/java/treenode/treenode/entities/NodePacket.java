package treenode.treenode.entities;

import treenode.protocol.NodeProtocol;
import treenode.protocol.PackageTypeNotFoundException;
import treenode.protocol.Packet;
import treenode.treenode.TreeNode;

public class NodePacket {
    private final Connection connection;
    private final Packet packet;
    private long birth;

    public NodePacket(Connection connection, Packet packet) {
        this.connection = connection;
        this.packet = packet;
        birth = System.currentTimeMillis();
    }

    public Connection getConnection() {
        return connection;
    }

    public Packet getPacket() {
        return packet;
    }


    public void updatetime() {
    }

    public boolean isOld() {
        try {
            NodeProtocol.PackageType type = packet.getPackageType();
            boolean isAck = type.equals(NodeProtocol.PackageType.MESSAGE_ACK) ||
                    type.equals(NodeProtocol.PackageType.CONNECTION_ACK) ||
                    type.equals(NodeProtocol.PackageType.PARENT_ACK);
            return (isAck && System.currentTimeMillis() - birth > TreeNode.ACKOLDMILLIS) || System.currentTimeMillis() - birth > TreeNode.ALLOLDMILLIS;
        } catch (PackageTypeNotFoundException ignored) {
        }
        return true;
    }
}
