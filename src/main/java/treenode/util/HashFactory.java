package treenode.util;

import java.util.Random;

public class HashFactory {
    private HashFactory() {
    }

    private static final Random random = new Random();

    public static final int HASHSIZE = 16;

    public static byte[] createChecksum() {
        byte[] res = new byte[HASHSIZE];
        random.nextBytes(res);
        return res;
    }
}