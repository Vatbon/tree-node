package treenode.util;

import treenode.protocol.NodeProtocol;
import treenode.protocol.PackageTypeNotFoundException;
import treenode.treenode.entities.Connection;
import treenode.treenode.entities.NodePacket;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NodeLogger {
    private class HistoryEntity {
        NodeProtocol.PackageType type;
        String message;
        long logTime;
        Connection connection;

        HistoryEntity(NodePacket nodePacket) {
            try {
                logTime = System.currentTimeMillis();
                type = nodePacket.getPacket().getPackageType();
                if (type.equals(NodeProtocol.PackageType.MESSAGE))
                    message = String.valueOf(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(nodePacket.getPacket().getBody())));
                connection = nodePacket.getConnection();
            } catch (PackageTypeNotFoundException ignored) {
            }
        }
    }

    private final short HISTORYSIZE = 1024;
    private List<HistoryEntity> historyEntityList = new ArrayList<>();
    private final OutputStream outputStream;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS ");

    public NodeLogger() {
        outputStream = System.out;
    }

    public NodeLogger(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * @param nodePacket
     * @param source     0 - to
     *                   1 - from
     */
    public void log(NodePacket nodePacket, int source) {
        if (historyEntityList.size() > HISTORYSIZE)
            historyEntityList.remove(0);
        HistoryEntity historyEntity = new HistoryEntity(nodePacket);
        historyEntityList.add(historyEntity);
        StringBuilder stringBuilder = new StringBuilder().append(dateFormat.format(new Date(historyEntity.logTime))).append(": ");
        if (source == 0)
            stringBuilder.append("->").append(historyEntity.connection.getInetAddress()).append(":").append(historyEntity.connection.getPort())
                    .append("[").append(historyEntity.type).append("]");
        if (source == 1)
            stringBuilder.append("<-").append(historyEntity.connection.getInetAddress()).append(":").append(historyEntity.connection.getPort())
                    .append("[").append(historyEntity.type).append("]");
        if (historyEntity.type.equals(NodeProtocol.PackageType.MESSAGE))
            stringBuilder.append(" [").append(historyEntity.message).append("]");
        try {
            outputStream.write(stringBuilder.append("\n").toString().getBytes());
        } catch (IOException ignored) {
        }
    }

    public List<HistoryEntity> getHistoryEntityList() {
        return historyEntityList;
    }
}
