import org.junit.Assert;
import org.junit.Test;
import treenode.treenode.entities.Connection;
import treenode.protocol.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class RunTest {
    private final byte[] nameTest = {0, 1, 2, 3};
    private final byte[] bodyTest = {2, 0, 1, 9};

    @Test
    public void testMessage() {
        try {
            Packet packet1 = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.MESSAGE, bodyTest, nameTest);
            Assert.assertNotNull(packet1);
            /*Check type*/
            Assert.assertSame(NodeProtocol.PackageType.MESSAGE, packet1.getPackageType());
            /*Check name*/
            Assert.assertArrayEquals(nameTest, packet1.getName());
            /*Check body*/
            Assert.assertArrayEquals(bodyTest, packet1.getBody());
        } catch (PackageTypeNotFoundException | EmptyByteArrayException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGuid(){
        try {
            Packet packetM = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.MESSAGE, bodyTest, nameTest);
            Assert.assertNotNull(packetM);
            Packet packetA = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.MESSAGE_ACK, packetM.getGuid(), null);
            Assert.assertNotNull(packetA);
            Assert.assertArrayEquals(packetM.getGuid(), packetA.getGuid());
        } catch (PackageTypeNotFoundException | EmptyByteArrayException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParent(){
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            int port = 12345;
            Packet packet = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT, inetAddress.getAddress(), ByteBuffer.allocate(4).putInt(port).array());
            Assert.assertNotNull(packet);
            Assert.assertArrayEquals(packet.getInetAddress(), inetAddress.getAddress());
            Assert.assertEquals(packet.getPort(), port);
        } catch (UnknownHostException | EmptyByteArrayException | PackageTypeNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConnection(){
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            int port = 12345;
            Connection connection = new Connection(inetAddress, port);
            Packet packet = NodeProtocolPackageFactory.makePackage(NodeProtocol.PackageType.PARENT, inetAddress.getAddress(), ByteBuffer.allocate(4).putInt(port).array());
            Assert.assertNotNull(packet);
            Connection connection1 = new Connection(InetAddress.getByAddress(packet.getInetAddress()), packet.getPort());
            Assert.assertEquals(connection, connection1);
            Assert.assertEquals(connection, connection.clone());
        } catch (UnknownHostException | EmptyByteArrayException | PackageTypeNotFoundException e) {
            e.printStackTrace();
        }
    }
}
